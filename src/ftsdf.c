

#include "ftcommon.h"
#include "common.h"
#include "output.h"
#include "mlgetopt.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


#ifndef   FALSE
#  define FALSE 0
#endif

#ifndef   TRUE
#  define TRUE 1
#endif


  static struct  status_
  {
    const char*  keys;
    const char*  dims;
    const char*  device;

    int          res;
    int          ptsize;

  } status = { "", DIM, NULL, 72, 48 };

  static FTDemo_Display*  display;
  static FTDemo_Handle*   handle;

  /*************************************************************************/
  /*************************************************************************/
  /*************************************************************************/
  /****                                                                 ****/
  /****                    U S A G E   S E C T I O N                    ****/
  /****                                                                 ****/
  /*************************************************************************/
  /*************************************************************************/
  /*************************************************************************/

  static void
  usage( char*  execname )
  {
    fprintf( stderr,
      "\n"
      "ftsdf: distance field viewer -- part of the FreeType project\n"
      "------------------------------------------------------------\n"
      "\n" );
    fprintf( stderr,
      "Usage: %s [options] pt font ...\n"
      "\n",
             execname );
    fprintf( stderr,
      "  pt        The point size for the given resolution.\n"
      "            If the resolution is 72dpi, this directly gives the\n"
      "            ppem value (pixel per EM).\n" );
    fprintf( stderr,
      "  font      The font file to display.\n"
      "\n" );
    fprintf( stderr,
      "  -d WxH[xD]\n"
      "            Set the window width, height, and color depth\n"
      "            (default: 640x480x24).\n"
      "  -k keys   Emulate sequence of keystrokes upon start-up.\n"
      "            If the sequence contain `q', use \"batch\" mode.\n"
      "  -r R      Use resolution R dpi (default: 72dpi).\n"
      "\n"
      "  -v        Show version.\n"
      "\n" );

    exit( 1 );
  }


  static void
  parse_cmdline( int*     argc,
                 char***  argv )
  {
    char*  execname;
    int    option;


    execname = ft_basename( (*argv)[0] );

    while ( 1 )
    {
      option = getopt( *argc, *argv, "d:k:r:v" );

      if ( option == -1 )
        break;

      switch ( option )
      {
      case 'd':
        status.dims = optarg;
        break;

      case 'k':
        status.keys = optarg;
        while ( *optarg && *optarg != 'q' )
          optarg++;
        if ( *optarg == 'q' )
          status.device = "batch";
        break;

      case 'r':
        status.res = atoi( optarg );
        if ( status.res < 1 )
          usage( execname );
        break;

      case 'v':
        {
          FT_String str[64] = "ftsdf (FreeType) ";


          FTDemo_Version( handle, str );
          printf( "%s\n", str );
          exit( 0 );
        }
        /* break; */

      default:
        usage( execname );
        break;
      }
    }

    *argc -= optind;
    *argv += optind;

    if ( *argc <= 1 )
      usage( execname );

    status.ptsize = (int)( atof( *argv[0] ) * 64.0f );
    if ( status.ptsize == 0 )
      status.ptsize = 64;

    (*argc)--;
    (*argv)++;
  }


  /*************************************************************************/
  /*************************************************************************/
  /*************************************************************************/
  /****                                                                 ****/
  /****                   E V E N T   H A N D L I N G                   ****/
  /****                                                                 ****/
  /*************************************************************************/
  /*************************************************************************/
  /*************************************************************************/


  static void
  event_help( void )
  {
    char     buf[256];
    char     version[64] = "";

    grEvent  dummy_event;


    FTDemo_Version( handle, version );


    FTDemo_Display_Clear( display );
    grSetLineHeight( 10 );
    grGotoxy( 0, 0 );
    grSetMargin( 2, 1 );
    grGotobitmap( display->bitmap );

    snprintf( buf, sizeof( buf ),
              "FreeType Distance Field Viewer - part of the FreeType %s"
              " test suite", version );

    grWriteln( buf );
    grLn();
    grWriteln( "This program is used to display signed distance field," );
    grWriteln( "generated using the `sdf' module of the FreeType library." );
    grLn();
    grWriteln( "Use the following keys :" );
    grLn();
    grWriteln( "  F1 or ?     : display this help screen" );
    grLn();
    grWriteln( "  q, ESC      : quit" );
    grLn();
    grWriteln( "press any key to exit this help screen" );

    grRefreshSurface( display->surface );
    grListenSurface( display->surface, gr_event_key, &dummy_event );
  }


  static void
  event_color_change( void )
  {
    display->back_color = grFindColor( display->bitmap,  0,  0,  0, 0xff );
    display->fore_color = grFindColor( display->bitmap, ~0, ~0, ~0, 0xff );
  }


  static void
  event_font_change( int  delta )
  {
    FTDemo_Set_Current_Font( handle, handle->fonts[0] );
    FTDemo_Set_Current_Charsize( handle, status.ptsize, status.res );
  }


  static int
  Process_Event( void )
  {
    grEvent  event;
    int      ret = FALSE;


    if ( *status.keys )
      event.key = grKEY( *status.keys++ );
    else
    {
      grListenSurface( display->surface, 0, &event );

      if ( event.type == gr_event_resize )
        return ret;
    }

    switch ( event.key )
    {
    case grKeyEsc:
    case grKEY( 'q' ):
      ret = TRUE;
      goto Exit;
    case grKeyF1:
    case grKEY( '?' ):
      event_help();
      goto Exit;

    default:
      break;
    }

  Exit:
    return ret;
  }


  static void
  write_header( FT_Error  error_code )
  {
    /* TODO */

    grRefreshSurface( display->surface );
  }


  static int
  Render_Character( void )
  {
    int x = 320, y = 240;

    FT_Size size;
    FT_Glyph glyph;

    FTDemo_Get_Size( handle, &size );
    FT_Load_Glyph( size->face, 0, FT_LOAD_DEFAULT );
    FT_Render_Glyph( size->face->glyph, FT_RENDER_MODE_SDF );

    FT_Get_Glyph( size->face->glyph, &glyph );

    FTDemo_Draw_Glyph( handle, display, glyph, &x, &y );

    return error;
  }


  int
  main( int     argc,
        char**  argv )
  {
    handle = FTDemo_New(); /* initialize the demo engine */

    parse_cmdline( &argc, &argv );

    for ( ; argc > 0; argc--, argv++ )
    {
      error = FTDemo_Install_Font( handle, argv[0], 0, 0 );

      if ( error )
      {
        fprintf( stderr, "failed to install %s", argv[0] );
        if ( error == FT_Err_Invalid_CharMap_Handle )
          fprintf( stderr, ": missing valid charmap\n" );
        else
          fprintf( stderr, "\n" );
      }
    }

    if ( handle->num_fonts == 0 )
      PanicZ( "could not open any font file(s)" );

    display = FTDemo_Display_New( status.device, status.dims );

    if ( !display )
      PanicZ( "could not allocate display surface" );

    grSetTitle( display->surface,
                "FreeType Distance Field Viewer - press ? for help" );
    FTDemo_Icon( handle, display );

    event_color_change(); /* set background and text color */
    event_font_change( 0 );

    do
    {
      FTDemo_Display_Clear( display );


      error = Render_Character();

      write_header( error );
    } while ( !Process_Event() );

    printf( "Execution completed successfully.\n" );

    FTDemo_Display_Done( display );
    FTDemo_Done( handle );
    exit( 0 );      /* for safety reasons */

    /* return 0; */ /* never reached */
  }

/* end */
